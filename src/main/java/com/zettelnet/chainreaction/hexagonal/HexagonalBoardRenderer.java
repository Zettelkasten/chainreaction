package com.zettelnet.chainreaction.hexagonal;

import java.awt.Color;
import java.awt.Graphics2D;

import com.zettelnet.chainreaction.BoardRenderer;
import com.zettelnet.chainreaction.Player;

public class HexagonalBoardRenderer implements BoardRenderer<HexagonalTile> {

	private final HexagonalBoard board;

	final int CENTER_X, CENTER_Y;

	final int PADDING;

	final int TILE_SIZE; // the radius of a hexagon

	final int BUBBLE_PADDING;
	final int BUBBLE_SIZE;

	public HexagonalBoardRenderer(HexagonalBoard board, int screenWidth, int screenHeight) {
		this.board = board;
		// @todo what if screen size changes ingame?

		this.CENTER_X = screenWidth / 2;
		this.CENTER_Y = screenHeight / 2;

		this.PADDING = 30;
		this.TILE_SIZE = Math.min((screenWidth - PADDING * 2) / (board.getRadius() * 2 + 1),
				(screenHeight - PADDING * 2) / (board.getRadius() * 2 + 1)) / 2;

		this.BUBBLE_PADDING = TILE_SIZE / 2;
		this.BUBBLE_SIZE = (int) (BUBBLE_PADDING * 0.3);
	}

	public void paint(Graphics2D g) {
		for (int bx = -board.getRadius(); bx <= board.getRadius(); bx++) {
			for (int by = -board.getRadius(); by <= board.getRadius(); by++) {
				int bz = -(bx + by);
				if (Math.abs(bz) > board.getRadius()) {
					// @todo kinda wastefun
					continue;
				}

				HexagonalTile tile = new HexagonalTile(bx, by);
				Player color = board.getBoardColor(tile);
				int amount = board.getBoardAmount(tile);

				double px = (CENTER_X + (double) bx * TILE_SIZE * Math.sqrt(3)
						+ (double) by * TILE_SIZE * Math.sqrt(3) / 2);
				double py = (CENTER_Y + (double) by * TILE_SIZE * 3 / 2);

				if (color != null) {
					g.setColor(color.getColor());
					double rm = TILE_SIZE / 2;

					// draw bubbles
					for (int m = 0; m < amount; m++) {
						int mx = (int) (px + rm * Math.cos(Math.PI * 30 / 180 + 2 * Math.PI * m / 6));
						int my = (int) (py + rm * Math.sin(Math.PI * 30 / 180 + 2 * Math.PI * m / 6));

						g.fillOval(mx - BUBBLE_SIZE, my - BUBBLE_SIZE, BUBBLE_SIZE * 2, BUBBLE_SIZE * 2);
					}
				} else {
					g.setColor(Color.BLACK);
				}
//				 g.drawString(bx + ":" + by + ":" + bz, (int) px - 13, (int) py + 4);
//				 g.drawOval((int) (px - TILE_SIZE), (int) (py - TILE_SIZE), TILE_SIZE * 2, TILE_SIZE * 2);

				double r = TILE_SIZE;

				// draw hexagon
				for (int i = 0; i < 6; i++) {
					int j = (i + 1) % 6;

					int ix = (int) (px + r * Math.cos(Math.PI * 30 / 180 + 2 * Math.PI * i / 6) + 0.5);
					int iy = (int) (py + r * Math.sin(Math.PI * 30 / 180 + 2 * Math.PI * i / 6) + 0.5);
					int jx = (int) (px + r * Math.cos(Math.PI * 30 / 180 + 2 * Math.PI * j / 6) + 0.5);
					int jy = (int) (py + r * Math.sin(Math.PI * 30 / 180 + 2 * Math.PI * j / 6) + 0.5);

					g.drawLine(ix, iy, jx, jy);
				}
			}
		}
	}

	public HexagonalTile getTileAtMouse(int mx, int my) {
		mx -= CENTER_X;
		my -= CENTER_Y;

		double bx = (Math.sqrt(3) / 3 * mx - 1D / 3 * my) / TILE_SIZE;
		double by = (2D / 3 * my) / TILE_SIZE;
		double bz = -(bx + by);

		int rx = (int) Math.round(bx);
		int ry = (int) Math.round(by);
		int rz = (int) Math.round(bz);

		double x_diff = Math.abs(rx - bx);
		double y_diff = Math.abs(ry - by);
		double z_diff = Math.abs(rz - bz);

		if (x_diff > y_diff && x_diff > z_diff) {
			rx = -ry - rz;
		} else if (y_diff > z_diff) {
			ry = -rx - rz;
		} else {
			rz = -rx - ry;
		}

		if (Math.abs(rx) > board.getRadius() || Math.abs(ry) > board.getRadius() || Math.abs(rz) > board.getRadius()) {
			return null;
		} else {
			return new HexagonalTile((int) rx, (int) ry);
		}
	}
}
