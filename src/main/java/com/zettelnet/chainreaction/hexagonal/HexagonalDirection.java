package com.zettelnet.chainreaction.hexagonal;

public enum HexagonalDirection {

	UP_LEFT(0, 1, -1), UP_RIGHT(1, 0, -1), LEFT(-1, 1, 0), RIGHT(1, -1, 0), DOWN_LEFT(-1, 0, 1), DOWN_RIGHT(0, -1, 1);

	private final int xmod, ymod, zmod;

	private HexagonalDirection(final int xmod, final int ymod, final int zmod) {
		this.xmod = xmod;
		this.ymod = ymod;
		this.zmod = zmod;
	}

	public int getX() {
		return xmod;
	}

	public int getY() {
		return ymod;
	}

	public int getZ() {
		return zmod;
	}
}
