package com.zettelnet.chainreaction.hexagonal;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.zettelnet.chainreaction.Board;
import com.zettelnet.chainreaction.Player;

public class HexagonalBoard implements Board<HexagonalTile> {

	private final int radius;

	private final Map<HexagonalTile, Player> boardColors;
	private final Map<HexagonalTile, Integer> boardAmounts;

	public HexagonalBoard(final int radius) {
		this.radius = radius;

		this.boardColors = new HashMap<>();
		this.boardAmounts = new HashMap<>();
	}

	@Override
	public Player getBoardColor(HexagonalTile tile) {
		return boardColors.get(tile);
	}

	@Override
	public int getBoardAmount(HexagonalTile tile) {
		return boardAmounts.getOrDefault(tile, 0);
	}

	@Override
	public int getBoardCapacity(HexagonalTile tile) {
		int large = 0;
		if (Math.abs(tile.getX()) >= 3) {
			large++;
		}
		if (Math.abs(tile.getY()) >= 3) {
			large++;
		}
		if (Math.abs(tile.getZ()) >= 3) {
			large++;
		}

		if (large == 0) {
			return 6;
		} else if (large == 1) {
			return 4;
		} else {
			return 3;
		}
	}

	@Override
	public void setBoard(HexagonalTile tile, int amount, Player color) {
		boardColors.put(tile, color);
		boardAmounts.put(tile, amount);
	}

	@Override
	public Set<HexagonalTile> getNeighbors(HexagonalTile tile) {
		Set<HexagonalTile> neighbors = new HashSet<>(4);
		for (HexagonalDirection dir : HexagonalDirection.values()) {
			int cx = tile.getX() + dir.getX();
			int cy = tile.getY() + dir.getY();
			int cz = tile.getZ() + dir.getZ();

			if (Math.max(Math.max(Math.abs(cx), Math.abs(cy)), Math.abs(cz)) > radius) {
				continue;
			}
			neighbors.add(new HexagonalTile(cx, cy));
		}
		return neighbors;
	}

	public int getRadius() {
		return radius;
	}

}
