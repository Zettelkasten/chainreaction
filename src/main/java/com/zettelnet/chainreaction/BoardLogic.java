package com.zettelnet.chainreaction;

import java.util.ArrayList;
import java.util.List;

public class BoardLogic<T> {
	
	private final Board<T> board;
	
	public BoardLogic(final Board<T> board) {
		this.board = board;
	}

	public boolean increaseField(T tile, Player color) {
		if (board.getBoardAmount(tile) >= board.getBoardCapacity(tile)) {
			return false;
		}

		board.setBoard(tile, board.getBoardAmount(tile) + 1, color);

		reactQueue.add(tile);
		return true;
	}

	private final List<T> reactQueue = new ArrayList<>();

	public void doSimulation() {
		if (reactQueue.isEmpty()) {
			return;
		}

		@SuppressWarnings("unchecked")
		T[] queue = (T[]) reactQueue.toArray();
		reactQueue.clear();
		for (T tile : queue) {
			simulate(tile);
		}
	}

	public void simulate(T tile) {
		if (board.getBoardAmount(tile) < board.getBoardCapacity(tile)) {
			return;
		} else {
			Player player = board.getBoardColor(tile);
			// react
			board.setBoard(tile, 0, null);

			for (T neighbor : board.getNeighbors(tile)) {
				increaseField(neighbor, player);
			}
		}
	}
}
