package com.zettelnet.chainreaction;

import java.util.Set;

public interface Board<T> {

	public Player getBoardColor(T tile);

	public int getBoardAmount(T tile);

	public int getBoardCapacity(T tile);

	public void setBoard(T tile, int amount, Player color);

	public Set<T> getNeighbors(T tile);
}
