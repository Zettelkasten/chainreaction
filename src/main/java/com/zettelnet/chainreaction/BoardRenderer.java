package com.zettelnet.chainreaction;

import java.awt.Graphics2D;

public interface BoardRenderer<T> {

	public void paint(Graphics2D g);

	public T getTileAtMouse(int mx, int my);
}
