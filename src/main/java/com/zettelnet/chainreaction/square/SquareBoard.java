package com.zettelnet.chainreaction.square;

import java.util.HashSet;
import java.util.Set;

import com.zettelnet.chainreaction.Board;
import com.zettelnet.chainreaction.Player;

public class SquareBoard implements Board<SquareTile> {

	private int width, height;
	private Player[][] boardColors;
	private int[][] boardAmounts;

	public SquareBoard(int width, int height) {
		this.width = width;
		this.height = height;

		this.boardColors = new Player[width][height];
		this.boardAmounts = new int[width][height];
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	@Override
	public Player getBoardColor(SquareTile tile) {
		return boardColors[tile.getX()][tile.getY()];
	}

	@Override
	public int getBoardAmount(SquareTile tile) {
		return boardAmounts[tile.getX()][tile.getY()];
	}

	@Override
	public int getBoardCapacity(SquareTile tile) {
		int bx = tile.getX();
		int by = tile.getY();

		if ((bx == 0 || bx == width - 1) && (by == 0 || by == height - 1)) {
			// corner
			return 2;
		} else if ((bx == 0 || bx == width - 1) || (by == 0 || by == height - 1)) {
			// edge
			return 3;
		} else {
			// center
			return 4;
		}
	}

	@Override
	public void setBoard(SquareTile tile, int amount, Player color) {
		boardAmounts[tile.getX()][tile.getY()] = amount;
		boardColors[tile.getX()][tile.getY()] = color;
	}

	@Override
	public Set<SquareTile> getNeighbors(SquareTile tile) {
		Set<SquareTile> neighbors = new HashSet<>(4);
		for (SquareDirection dir : SquareDirection.values()) {
			int cx = tile.getX() + dir.getX();
			int cy = tile.getY() + dir.getY();

			if (cx < 0 || cx >= width || cy < 0 || cy >= height) {
				continue;
			}
			neighbors.add(new SquareTile(cx, cy));
		}
		return neighbors;
	}
}
