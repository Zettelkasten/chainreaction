package com.zettelnet.chainreaction.square;

public enum SquareDirection {

	LEFT(-1, 0), RIGHT(1, 0), UP(0, -1), DOWN(0, 1);
	
	private final int xmod, ymod;
	
	private SquareDirection(final int xmod, final int ymod) {
		this.xmod = xmod;
		this.ymod = ymod;
	}
	
	public int getX() {
		return xmod;
	}
	public int getY() {
		return ymod;
	}
}