package com.zettelnet.chainreaction.square;

import java.awt.Color;
import java.awt.Graphics2D;

import com.zettelnet.chainreaction.BoardRenderer;
import com.zettelnet.chainreaction.Player;

public class SquareBoardRenderer implements BoardRenderer<SquareTile> {

	private final SquareBoard board;

	final int PADDING;
	final int TILE_WIDTH, TILE_HEIGHT;

	final int BUBBLE_PADDING;
	final int BUBBLE_SIZE;

	public SquareBoardRenderer(SquareBoard board, int screenWidth, int screenHeight) {
		this.board = board;
		// @todo what if screen size changes ingame?
		
		this.PADDING = 30;
		this.TILE_WIDTH = (screenWidth - PADDING * 2) / board.getWidth();
		this.TILE_HEIGHT = (screenHeight - PADDING * 2) / board.getHeight();
		
		this.BUBBLE_PADDING = TILE_WIDTH / 2;
		this.BUBBLE_SIZE = (int) (BUBBLE_PADDING * 0.6);
	}

	public void paint(Graphics2D g) {
		for (int bx = 0; bx < board.getWidth(); bx++) {
			for (int by = 0; by < board.getHeight(); by++) {
				SquareTile tile = new SquareTile(bx, by);
				Player color = board.getBoardColor(tile);
				int amount = board.getBoardAmount(tile);

				if (color != null) {
					g.setColor(color.getColor());
					for (int m = 0; m < amount; m++) {
						int mx = m % 2;
						int my = m / 2;

						int cx = PADDING + bx * TILE_WIDTH + BUBBLE_PADDING / 2 + mx * BUBBLE_PADDING;
						int cy = PADDING + by * TILE_HEIGHT + BUBBLE_PADDING / 2 + my * BUBBLE_PADDING;
						g.fillOval(cx - BUBBLE_SIZE / 2, cy - BUBBLE_SIZE / 2, BUBBLE_SIZE, BUBBLE_SIZE);
					}
				} else {
					g.setColor(Color.BLACK);
				}
				g.drawRect(PADDING + bx * TILE_WIDTH, PADDING + by * TILE_HEIGHT, TILE_WIDTH, TILE_HEIGHT);
			}
		}
	}

	public SquareTile getTileAtMouse(int mx, int my) {
		int bx = (mx - PADDING) / TILE_WIDTH;
		int by = (my - PADDING) / TILE_HEIGHT;
		
		if (bx >= 0 && bx < board.getWidth() && by >= 0 && by < board.getHeight()) {
			return new SquareTile(bx, by);
		} else {
			return null;
		}
	}
}
