package com.zettelnet.chainreaction;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

import com.zettelnet.chainreaction.hexagonal.HexagonalBoard;
import com.zettelnet.chainreaction.hexagonal.HexagonalBoardRenderer;
import com.zettelnet.chainreaction.hexagonal.HexagonalTile;

public class Chainreaction extends JFrame implements KeyListener, MouseListener {
	private static final long serialVersionUID = 0L;

	public static void main(String[] args) {
		new Chainreaction();
	}

	public static enum State {
		MENU, INGAME;
	}

	private boolean running = true;
	private State state = State.MENU;

	// INGAME STATE
	private Board<HexagonalTile> board;
	private BoardLogic<HexagonalTile> boardLogic;
	private BoardRenderer<HexagonalTile> boardRenderer;

	private Player currentPlayer;

	public Chainreaction() {
		super("Chainreaction");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(512, 512);
		setIgnoreRepaint(true);
		setResizable(false);
		setVisible(true);
		createBufferStrategy(2);

		addKeyListener(this);
		addMouseListener(this);

		System.out.println("Running client!");

		while (running) {
			try {
				Thread.sleep(100);
				update();
				repaint();
			} catch (InterruptedException e) {
				e.printStackTrace();
				running = false;
			}
		}
	}

	@Override
	public void paint(Graphics graphics) {
		Graphics2D g = (Graphics2D) getBufferStrategy().getDrawGraphics();

		g.clearRect(0, 0, getWidth(), getHeight());
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		switch (state) {
		case MENU:
			g.setFont(new Font("Courier New", Font.BOLD, 30));
			g.setColor(Color.GRAY);
			g.drawString("Chainreaction!", 100 - 1, 200 - 1);
			break;
		case INGAME:
			g.setColor(currentPlayer.getBackgroundColor());
			g.fillRect(0, 0, getWidth(), getHeight());

			boardRenderer.paint(g);
			break;
		}

		getBufferStrategy().show();
	}

	public void update() {
		switch (state) {
		case INGAME:
			boardLogic.doSimulation();
		default:
			break;
		}
	}

	@Override
	public void keyPressed(KeyEvent event) {
		switch (state) {
		case MENU:
			switch (event.getKeyCode()) {
			case KeyEvent.VK_SPACE:
				// initialize state stuff
				state = State.INGAME;
				HexagonalBoard newBoard = new HexagonalBoard(3);
				board = newBoard;
				boardLogic = new BoardLogic<>(board);
//				boardRenderer = new BoardRenderer(board, getWidth(), getHeight());
				boardRenderer = new HexagonalBoardRenderer(newBoard, getWidth(), getHeight());
				currentPlayer = Player.values()[0];
				break;
			}
			break;
		default:
			break;
		}
		
		if (event.getKeyCode() == KeyEvent.VK_F) {
			this.setExtendedState(JFrame.MAXIMIZED_BOTH); 
			this.setUndecorated(true);
			this.setVisible(true);
		}
	}

	@Override
	public void keyReleased(KeyEvent event) {
	}

	@Override
	public void keyTyped(KeyEvent event) {
	}

	@Override
	public void mouseClicked(MouseEvent event) {
	}

	@Override
	public void mouseEntered(MouseEvent event) {
	}

	@Override
	public void mouseExited(MouseEvent event) {
	}

	@Override
	public void mousePressed(MouseEvent event) {
		switch (state) {
		case INGAME:
			int mx = event.getX();
			int my = event.getY();

			HexagonalTile clicked = boardRenderer.getTileAtMouse(mx, my);
			if (clicked != null) {
				if (board.getBoardColor(clicked) == null || board.getBoardColor(clicked) == currentPlayer) {
					boolean changed = boardLogic.increaseField(clicked, currentPlayer);
					if (changed) {
						currentPlayer = getNextPlayer(currentPlayer);
					}
				}
			}
		default:
			break;
		}
	}

	@Override
	public void mouseReleased(MouseEvent event) {
	}

	public Player getNextPlayer(Player player) {
		return Player.values()[(player.ordinal() + 1) % Player.values().length];
	}
}
