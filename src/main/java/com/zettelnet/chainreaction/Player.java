package com.zettelnet.chainreaction;

import java.awt.Color;

public enum Player {
	
	BLUE(Color.BLUE, Color.CYAN), RED(Color.RED, Color.PINK);
	
	private Color color;
	private Color backgroundColor;
	
	private Player(final Color color, final Color backgroundColor) {
		this.color = color;
		this.backgroundColor = backgroundColor;
	}
	
	public Color getColor() {
		return color;
	}
	
	public Color getBackgroundColor() {
		return backgroundColor;
	}
}
